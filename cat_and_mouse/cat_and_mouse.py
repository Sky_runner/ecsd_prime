def cat_and_mouse(x, y, z):
    cat_a_dist = abs(z - x)
    cat_b_dist = abs(z - y)

    if cat_a_dist < cat_b_dist:
        return "Cat A"
    elif cat_b_dist < cat_a_dist:
        return "Cat B"
    elif cat_a_dist == cat_b_dist:
        return "Mouse C"
