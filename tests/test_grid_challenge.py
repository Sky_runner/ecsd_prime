from production.grid_challenge import grid_challenge
import unittest


class TwoCharacterTest(unittest.TestCase):
    def test_give_grid_ebacd_fghig_olmkn_should_YES(self):
        grid = ["ebacd", "fghij", "olmkn", "trpqs", "xywuv"]
        expected_result = "YES"

        result = grid_challenge(grid)

        self.assertEqual(result, expected_result)

    def test_give_a_xyz_xyz_should_NO(self):
        grid = ["xyz", "xyz", "hmp"]
        expected_result = "NO"

        result = grid_challenge(grid)

        self.assertEqual(result, expected_result)
