from production.super_fizzbuzz import (
    FizzBuzz,
    SuperFizzBuzz,
)
import unittest


class SuperFizzBuzzTest(unittest.TestCase):
    def test_give_3_should_fizz_(self):
        num = 3
        expected_result = "Fizz"

        result = FizzBuzz(num)

        self.assertEqual(expected_result, result.check())

    def test_give_5_should_buzz(self):
        num = 5
        expected_result = "Buzz"

        result = FizzBuzz(num)

        self.assertEqual(expected_result, result.check())

    def test_give_15_should_fizzbuzz(self):
        num = 15
        expected_result = "FizzBuzz"

        result = FizzBuzz(num)

        self.assertEqual(expected_result, result.check())

    def test_give_25_should_buzzbuzz(self):
        num = 25
        expected_result = "BuzzBuzz"

        result = FizzBuzz(num)

        self.assertEqual(expected_result, result.check())

    def test_give_9_should_fizzfizz(self):
        num = 9
        expected_result = "FizzFizz"

        result = FizzBuzz(num)

        self.assertEqual(expected_result, result.check())

    def test_give_225_should_fizzfizzbuzzbuzz(self):
        num = 225
        expected_result = "FizzFizzBuzzBuzz"

        result = FizzBuzz(num)

        self.assertEqual(expected_result, result.check())

    def test_give_2_should_no_fizzbuzz(self):
        num = 2
        expected_result = "NoFizzBuzz"

        result = FizzBuzz(num)

        self.assertEqual(expected_result, result.check())

    def test_give_0_should_no_fizzbuzz(self):
        num = 0
        expected_result = "FizzFizzBuzzBuzz"

        result = FizzBuzz(num)

        self.assertEqual(expected_result, result.check())

    def test_give_10000_should_buzzbuzz(self):
        num = 10000
        expected_result = "BuzzBuzz"

        result = FizzBuzz(num)

        self.assertEqual(expected_result, result.check())

    def test_give_9999_should_fizzfizz(self):
        num = 9999
        expected_result = "FizzFizz"

        result = FizzBuzz(num)

        self.assertEqual(expected_result, result.check())

    def test_give_5000_should_buzzbuzz(self):
        num = 5000
        expected_result = "BuzzBuzz"

        result = FizzBuzz(num)

        self.assertEqual(expected_result, result.check())

    def test_give_625_should_buzzbuzz(self):
        num = 625
        expected_result = "BuzzBuzz"

        result = FizzBuzz(num)

        self.assertEqual(expected_result, result.check())

    def test_give_1_should_no_fizz_buzz(self):
        num = 1
        expected_result = "NoFizzBuzz"

        result = FizzBuzz(num)

        self.assertEqual(expected_result, result.check())

    def test_give_9998_should_no_fizz_buzz(self):
        num = 9998
        expected_result = "NoFizzBuzz"

        result = FizzBuzz(num)

        self.assertEqual(expected_result, result.check())

    def test_give_5625_should_no_fizzfizzbuzzbuzz(self):
        num = 5625
        expected_result = "FizzFizzBuzzBuzz"

        result = FizzBuzz(num)

        self.assertEqual(expected_result, result.check())

    def test_give_9995_should_buzz(self):
        num = 9995
        expected_result = "Buzz"

        result = FizzBuzz(num)

        self.assertEqual(expected_result, result.check())

    def test_give_3390_should_fizzbuzz(self):
        num = 3390
        expected_result = "FizzBuzz"

        result = FizzBuzz(num)

        self.assertEqual(expected_result, result.check())

    # def test_give_0_should_fasle(self):
    #     num = 0

    #     result = FizzFizzBuzzBuzz

    #     self.assertFalse(result)

    # def test_give_1_should_not_fizz_buzz(self):
    #     num = 1
    #     expected_result = "NotFizzBuzz"

    #     result = not_fizz_buzz(num)

    #     self.assertEqual(expected_result, result)

    # def test_give_2_should_not_fizz_buzz(self):
    #     num = 2
    #     expected_result = "NotFizzBuzz"

    #     result = not_fizz_buzz(num)

    #     self.assertEqual(expected_result, result)

    # def test_give_999_should_fizz(self):
    #     num = 999
    #     expected_result = "Fizz"

    #     result = fizz(num)

    #     self.assertEqual(expected_result, result)

    # def test_give_1000_should_fizz(self):
    #     num = 1000
    #     expected_result = "Buzz"

    #     result = buzz(num)

    #     self.assertEqual(expected_result, result)

    # def test_give_998_should_not_fizz_buzz(self):
    #     num = 998
    #     expected_result = "NotFizzBuzz"

    #     result = not_fizz_buzz(num)

    #     self.assertEqual(expected_result, result)

    # def test_give_500_should_buzz(self):
    #     num = 500
    #     expected_result = "Buzz"

    #     result = buzz(num)

    #     self.assertEqual(expected_result, result)
