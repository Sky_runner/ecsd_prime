from production.two_character import two_character
import unittest


class TwoCharacterTest(unittest.TestCase):
    def test_give_beabeefeab_should_5(self):
        string = "beabeefeab"
        expected_result = "5"

        result = two_character(string)

        self.assertEqual(result, expected_result)

    def test_give_a_should_0(self):
        string = "a"
        expected_result = "0"

        result = two_character(string)

        self.assertEqual(result, expected_result)

    def test_give_ab_should_2(self):
        string = "ab"
        expected_result = "2"

        result = two_character(string)

        self.assertEqual(result, expected_result)
