from production.caesar_cipher import caesar_cipher
import unittest


class CaesarCipherTest(unittest.TestCase):
    def test_give_rachata_key_240244_should_veglexe(self):
        string = "rachata"
        expected_result = "veglexe"

        result = caesar_cipher(string, 240244)

        self.assertEqual(result, expected_result)

    def test_give_ahaheelo_key_240244_should_vcvczzgj(self):
        string = "ahaheelo"
        expected_result = "vcvczzgj"

        result = caesar_cipher(string, 21)

        self.assertEqual(result, expected_result)

    def test_give_calculator_step_2_should_ecnewncvqt(self):
        string = "middle-Outz"
        expected_result = "okffng-Qwvb"

        result = caesar_cipher(string, 2)

        self.assertEqual(result, expected_result)
