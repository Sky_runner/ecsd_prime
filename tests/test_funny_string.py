from production.funny_string import funny_string
import unittest


class FunnyStringTest(unittest.TestCase):
    def test_give_acxz_should_Funny(self):
        string = "acxz"
        expected_result = "Funny"

        result = funny_string(string)

        self.assertEqual(result, expected_result)

    def test_give_bcxz_should_not_Funny(self):
        string = "bcxz"
        expected_result = "Not Funny"

        result = funny_string(string)

        self.assertEqual(result, expected_result)

    def test_give_ivvkxq_should_Funny(self):
        string = "ivvkxq"
        expected_result = "Not Funny"

        result = funny_string(string)

        self.assertEqual(result, expected_result)
