from production.cat_and_mouse import cat_and_mouse
import unittest


class CatAndMouseTest(unittest.TestCase):
    def test_give_1_2_3_should_cat_b(self):
        cat_a = 1
        cat_b = 2
        mouse = 3
        result = cat_and_mouse(cat_a, cat_b, mouse)
        self.assertEqual(result, "Cat B")

    def test_give_1_3_2_should_mouse_c(self):
        cat_a = 1
        cat_b = 3
        mouse = 2
        result = cat_and_mouse(cat_a, cat_b, mouse)
        self.assertEqual(result, "Mouse C")

    def test_give_1_10_5_should_cat_a(self):
        cat_a = 1
        cat_b = 10
        mouse = 5
        result = cat_and_mouse(cat_a, cat_b, mouse)
        self.assertEqual(result, "Cat A")
