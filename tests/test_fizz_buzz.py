from production.fizzbuzzs import fizzbuzz, fizz, buzz, is_zero, not_fizz_buzz
import unittest


class FizzBuzzTest(unittest.TestCase):
    def test_give_15_should_fizz_buzz(self):
        num = 15
        expected_result = "FizzBuzz"

        result = fizzbuzz(num)

        self.assertEqual(expected_result, result)

    def test_give_3_should_fizz(self):
        num = 3
        expected_result = "Fizz"

        result = fizz(num)

        self.assertEqual(expected_result, result)

    def test_give_5_should_buzz(self):
        num = 5
        expected_result = "Buzz"

        result = buzz(num)

        self.assertEqual(expected_result, result)

    def test_give_0_should_fasle(self):
        num = 0

        result = is_zero(num)

        self.assertFalse(result)

    def test_give_1_should_not_fizz_buzz(self):
        num = 1
        expected_result = "NotFizzBuzz"

        result = not_fizz_buzz(num)

        self.assertEqual(expected_result, result)

    def test_give_2_should_not_fizz_buzz(self):
        num = 2
        expected_result = "NotFizzBuzz"

        result = not_fizz_buzz(num)

        self.assertEqual(expected_result, result)

    def test_give_999_should_fizz(self):
        num = 999
        expected_result = "Fizz"

        result = fizz(num)

        self.assertEqual(expected_result, result)

    def test_give_1000_should_fizz(self):
        num = 1000
        expected_result = "Buzz"

        result = buzz(num)

        self.assertEqual(expected_result, result)

    def test_give_998_should_not_fizz_buzz(self):
        num = 998
        expected_result = "NotFizzBuzz"

        result = not_fizz_buzz(num)

        self.assertEqual(expected_result, result)

    def test_give_500_should_buzz(self):
        num = 500
        expected_result = "Buzz"

        result = buzz(num)

        self.assertEqual(expected_result, result)
