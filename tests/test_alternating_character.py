from production.alternating_character import alternating_character
import unittest


class AlternatingCharacterTest(unittest.TestCase):
    def test_give_AAAA_should_3(self):
        string = "AAAA"
        expected_result = "3"

        result = alternating_character(string)

        self.assertEqual(result, expected_result)

    def test_give_BBBBB_should_4(self):
        string = "BBBBB"
        expected_result = "4"

        result = alternating_character(string)

        self.assertEqual(result, expected_result)

    def test_give_ABABABAB_should_0(self):
        string = "ABABABAB"
        expected_result = "0"

        result = alternating_character(string)

        self.assertEqual(result, expected_result)
