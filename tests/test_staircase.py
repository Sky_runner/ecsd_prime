from production.stair_case import staircase
import unittest


class StairCaseTest(unittest.TestCase):
    def test_give_2_with_hash_should_be_hh(self):
        num = 2
        text = "#"
        expected_output = " #\n" + "##\n"

        # act
        result = staircase(num, text)
        print(result)

        # assert
        self.assertEqual(result, expected_output, f"Should be {expected_output}")
