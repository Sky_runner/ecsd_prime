def staircase(n, text: str):
    result = ""
    for i in range(n):
        result += f"{text*(i+1):>{n}}\n"
    return result
