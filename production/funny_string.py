def funny_string(s):
    left, right = 0, len(s) - 1
    while right - left > 1:
        cur = abs(ord(s[left]) - ord(s[left + 1]))
        rcur = abs(ord(s[right]) - ord(s[right - 1]))

        if cur != rcur:
            return "Not Funny"
        left += 1
        right -= 1
    return "Funny"
