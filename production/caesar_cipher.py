def caesar_cipher(s, k):
    result = ""
    for i in s:
        if i.isalpha():
            a = "A" if i.isupper() else "a"
            result += chr(ord(a) + (ord(i) - ord(a) + k) % 26)
        else:
            result += i
    return result
