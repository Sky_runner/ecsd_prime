"""this is fizzbuzz production code"""


def fizzbuzz(number):
    """this is fizzbuzz"""
    if number % 3 != 0 and number % 5 != 0:
        return "!FizzBuzz"
    return "FizzBuzz"


def fizz(number):
    """this is fizzbuzz"""
    if number % 3 != 0:
        return "!Fizz"
    return "Fizz"


def buzz(number):
    """this is fizzbuzz"""
    if number % 5 != 0:
        return "!Buzz"
    return "Buzz"


def is_zero(number):
    """this is fizzbuzz"""
    if number == 0:
        return False
    return True


def not_fizz_buzz(number):
    """this is fizzbuzz"""
    if number % 3 != 0 and number % 5 != 0:
        return "NotFizzBuzz"
    return "FizzBuzz"
