def two_character(s):
    charSet = list(set(s))
    maxLength = 0
    for i in range(len(charSet) - 1):
        for j in range(i + 1, len(charSet)):
            a, b = charSet[i], charSet[j]
            temp_length = 0
            lastChar = ""
            for letter in s:
                if letter == a:
                    if lastChar == b or lastChar == "":
                        lastChar = a
                        temp_length += 1
                    elif lastChar == a:
                        break
                if letter == b:
                    if lastChar == a or lastChar == "":
                        lastChar = b
                        temp_length += 1
                    elif lastChar == b:
                        break
            else:
                if maxLength < temp_length:
                    maxLength = temp_length
    return maxLength
