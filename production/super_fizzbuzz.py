class SuperFizzBuzz:
    def __init__(self, number: float):
        self.number = number


class FizzBuzz(SuperFizzBuzz):
    def check(self):
        if self.number % 25 == 0 and self.number % 9 == 0:
            return "FizzFizzBuzzBuzz"
        elif self.number % 9 == 0:
            return "FizzFizz"
        elif self.number % 25 == 0:
            return "BuzzBuzz"
        elif self.number % 15 == 0:
            return "FizzBuzz"
        elif self.number % 3 == 0:
            return "Fizz"
        elif self.number % 5 == 0:
            return "Buzz"
        else:
            return "NoFizzBuzz"


# if __name__ == "__main__":
#     num = FizzBuzz(9998)
#     print(num.check())

# print(check_fizz)
